let squaresState;
let counter;
let occupiedSquares;
const victoryConditions = [
    [0, 1, 2], [0, 3, 6], [0, 4, 8], [3, 4, 5], [6, 7, 8], [2, 4, 6], [1, 4, 7], [2, 5, 8], [3, 4, 5]
]; // this is all of indexes of squares that combination can give a win

window.addEventListener('load', main);

function main() {
    // Initiate globals
    squaresState = document.querySelectorAll('.square');
    counter = 1;
    occupiedSquares = 0;
}

// Tic-Tac toggle classes and places X/0 symbols
document.querySelector('#mainsquare').addEventListener('click', onSquareClick);
function onSquareClick(event) {
        let clickedSquare = event.target;
        if (clickedSquare.classList.contains('tic') || clickedSquare.classList.contains('tac')) return;
        occupiedSquares++;

        switch (counter) {
            case 1: clickedSquare.classList.add('tic'); counter = 0; break;
            case 0: clickedSquare.classList.add('tac'); counter = 1; break;
        }
    counter == 1 ? document.querySelector('h2').innerHTML = 'Players turn: X' : document.querySelector('h2').innerHTML = 'Players turn: 0';
        checkVictoryConditions();
}



function checkVictoryConditions() {
    // I check all of victory combinations in cycle and searching classes
    for (let i = 0; i < victoryConditions.length; i++) {
        switch (true) {
            case compare(squaresState, victoryConditions[i], 'tic'): handleWinner('X', '#tic-win'); return;
            case compare(squaresState, victoryConditions[i], 'tac'): handleWinner('0', '#tac-win'); return;
        }
    }
    if (occupiedSquares == 9) { handleWinner('No one of', '#draw'); return; }
}

function compare(classesToCompare, victoryConditions, symbol) {
    let victory = false;
    // For each one victory combination i compare 3 square-classes. If they are similar - i make them as victory combination
    for (let i = 0; i < victoryConditions.length; i++) {
        const tempClassName = classesToCompare[victoryConditions[i]].classList.toString();

        if (!tempClassName.includes(symbol)) 
            return false;
        else victory = true;
    }
    return victory;
}

function handleWinner(winner, spanId) {

    // Show win-popup
    let winPopup = document.querySelector('.modal-window');
    winPopup.firstElementChild.innerHTML = winner;
    winPopup.classList.add('modal-active');

    // block the window
    let blockScreen = document.querySelector('.screen-block')
    blockScreen.classList.add('screen-block-active');
    blockScreen.addEventListener('click', (e) => { e.stopPropagation() });

    // Increase win-conter
    let b = +document.querySelector(spanId).innerHTML;
    document.querySelector(spanId).innerHTML = ++b;
    return;
}

// Close popup button event and handler
document.querySelector('.modal-window button').addEventListener('click', closePopup);
function closePopup() {
    this.parentElement.classList.remove('modal-active');
    document.querySelector('.screen-block').classList.remove('screen-block-active');
    setDefaults();
}

//RESET button event and handler
document.querySelector('#reset').addEventListener('click', setDefaults);
function setDefaults() {
    counter = 1;
    occupiedSquares = 0;
    document.querySelector('h2').innerHTML = 'Ходит: X'
    for (let square of squaresState) {
        square.setAttribute('class', 'square');
    }
}